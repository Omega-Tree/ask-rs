extern crate ask;

use ask::question::{Confirm, TraitQuestion};

fn main() {
    // let mut ask = ask::Ask::new();
    // ask.question(OpenEnded::new( "question?".to_string()));
    // ask.all();
    let mut confirm = Confirm::new("with cheese?".to_string());

    match confirm.send(){
        Ok(mut answer_collections) => {
            println!("");
            println!("{}", answer_collections.get_first_answer().unwrap()); //should be safe to unwrap on confirm
        },
        Err(error_str) => {panic!(error_str)}
    }


}