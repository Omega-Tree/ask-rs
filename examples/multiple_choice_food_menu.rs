extern crate ask;

use ask::question::{MultipleChoice, OptionGroup, TraitQuestion, QuestionOption};
use ask::validate;

fn main() {
    // let mut ask = ask::Ask::new();
    // ask.question(OpenEnded::new( "question?".to_string()));
    // ask.all();

    // let mut open_ended = OpenEnded::new("question?".to_string());
    // let answers = open_ended.send();

    let mut multiple_choice = MultipleChoice::new("What would you like to order?".to_owned());

    let answer_collections = multiple_choice
        .add_option_group(
            OptionGroup::new("Entrees")
                .add_option_by_str("Calamari".to_owned())
                .add_option_by_str("Buffalo wings".to_owned())
                .add_option_by_str("Shrimp".to_owned())
        )
        .add_option_group(
            OptionGroup::new("Main Course")
                .add_option(QuestionOption::new("Lobster".to_owned()))
                .add_option(QuestionOption::new("T-Bone".to_owned()).toggle_selected(None))//toggle the selected to opposite to what it currently is
                .add_option(QuestionOption::new("Burger".to_owned()).toggle_selected(Some(false)))
                .add_option(QuestionOption::new("Duck".to_owned()).toggle_selected(Some(true))) //set this option to be selected by default asking the question
        )
        .set_validation(Box::new(|answer_collection| {
                validate::group_options::selections_are_gtoe(& answer_collection, 0) &&
                validate::group_options::selections_are_ltoe(& answer_collection, 3)
            })
        )
        // .replace_continue_btn("CAN CONTINUE!!".to_owned(), "CANNOT CONTINUE!!".to_owned()) //can customize continue btn
        .send()
        .unwrap();


    println!("");

    // let answer_collections = multiple_choice.to_answer_collections(); //can pull the answer_collections at any time
    for answer_sets in answer_collections.answer_sets{
        for answer in answer_sets.answers{
            println!("{}", answer);
        }
    }
}