extern crate ask;

use ask::question::{OpenEnded, TraitQuestion};

fn main() {
    // let mut ask = ask::Ask::new();
    // ask.question(OpenEnded::new( "question?".to_string()));
    // ask.all();


    let mut open_ended = OpenEnded::new("question?".to_string());
    match open_ended.send(){
        Ok(answer_collections)  => {
            for answer_sets in answer_collections.answer_sets{
                for answer in answer_sets.answers{
                    println!("{}", answer);
                }
            }
        }
        Err(error_str) => {panic!(error_str)}
    }

}